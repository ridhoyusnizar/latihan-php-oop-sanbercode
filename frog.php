<?php
require_once 'animal.php';
class frog extends Animal
{
    public $legs = 4;
    public $cold_blooded = true;
    public function jump()
    {
        echo $this->name . " : hop hop" . "<br>";
    }
}
