<?php
require_once 'animal.php';
class ape extends Animal
{
    public $legs = 2;
    public $cold_blooded = true;
    public function yell()
    {
        echo $this->name . " : Auooo" . "<br>";
    }
}
