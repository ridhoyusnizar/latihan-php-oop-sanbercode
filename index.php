<?php
require_once 'animal.php';
require_once 'frog.php';
require_once 'ape.php';

$sheep = new Animal("shaun");
echo $sheep->get_name() . '<br>';

$sungokong = new Ape("kera sakti");
$sungokong->yell();

var_dump($sungokong) . '<br>';

$kodok = new Frog("buduk");
echo '<br>';
$kodok->jump();
var_dump($kodok) . '<br>';
